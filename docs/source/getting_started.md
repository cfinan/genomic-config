# Getting Started with genomic-config
__version__: `0.1.1a0`

The genomic-config package provides an interface to configuration files that provide the locations for various genomic based files that might exists on a system. It can be used by external programs and python code to provide a central point to locate the files. Currently, it only supports `ini` files but will be expanded to incorporate other configuration file types in the future.

genomic-config provides access to:

1. Reference genome assemblies (fasta files)
2. Reference genotype files (i.e. 1000 genomes)
3. Chain files for liftovers
4. Sample lists
5. variant annotation/mapping VCF files 

There is [online](https://cfinan.gitlab.io/genomic-config/index.html) documentation and offline PDF documentation can be downloaded [here](https://gitlab.com/cfinan/genomic-config/-/blob/master/resources/pdf/genomic-config.pdf).

## Installation instructions
At present, genomic-config is undergoing development and no packages exist yet on PyPy. I maintain a build in my [conda channel](https://anaconda.org/cfin) that I will try to keep updated to the latest Python versions back to Python 3.7.

With this in mind there are several installation options. 

### Installation using the conda build
This uses my personal conda channel and conda-forge to install. However, keep in mind to update regularly. 
```
conda install -c cfin -c conda-forge genomic-config
```

### Installation using git
First, clone this repository and then `cd` to the root of the repository.

```
git clone git@gitlab.com:cfinan/genomic-config.git
cd gwas_norm
```

#### Installation using git not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install genomic-config as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the genomic-config repository:
```
python -m pip install -e .
```

### Installation using conda dependencies
Several conda environments are provided in a `yaml` file in the directory `./resources/conda/env`. A new conda environment called `genomic_config_py3*` (where `*` is the Python version) can be built using the command:

```
# Example for python 3.8, from the root of the genomic-config repository
conda env create --file resources/conda/env/py38/conda_create.yml
```

To add to an existing environment use:
```
# Example for python 3.8, from the root of the genomic-config repository
conda env update --file resources/conda/env/py38/conda_update.yml
```

For an editable (developer) install run the command below from the root of the genomic-config repository:
```
python -m pip install -e .
```

For more information see `resources/conda/README.md`

## Next steps...
After installation, you may want to:

1. Run the tests using ``pytest ./tests`` - If you have installed using conda, you will need to clone the repository using git to run the tests.
2. [Setup](https://cfinan.gitlab.io/genomic-config/config_files.html) your configuration file.
