genomic_config.example_data sub-package
=======================================

genomic_config.example_data.examples
------------------------------------

.. automodule:: genomic_config.example_data.examples
   :members:
   :undoc-members:
   :show-inheritance:
