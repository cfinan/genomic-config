==================
genomic-config API
==================

.. toctree::
   :maxdepth: 4

   api/genomic_config
   api/example_data
