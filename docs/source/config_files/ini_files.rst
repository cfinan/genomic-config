=======================================
The genomic-config ``.ini`` file format
=======================================

Currently the ``.ini`` file format is the only format you can write your configuration file in. However, in many respects the ``.ini`` format is not the best format for this sort of configuration file, as the configuration is naturally hierarchical and the ``.ini`` format is naturally flat. However, it was chosen as the format to start with as it is very easy to hand code an ``.ini`` configuration file and let the parser determine the relationships.

Having said that, ``.yml``, ``.json`` will be added in future. The respective parsers for these will have the same interface and will be dropin replacements.

The basics of the ``.ini`` file format
=======================================

The ``.ini`` format is formed of section headings that are free text marked up with square brackets. For example: ``[this is a section]`` or ``[this.is.a.section]``. Within each section, there are one or more key value pairs representing the ``option key = option value``. Sections can't be duplicated, and within a section the option keys can't be duplicated.

The ``.ini`` format used in the configuration file mostly follows the standard ``.ini`` guidelines although there are a couple of differences that should be noted.

* In some sections, there are no ``key = value`` pairs, only values. This format is used if there are multiple values of the same type (for example filenames).
* Any ``key = value`` pairs must be separated by an equals sign (``=``) and not a colon (``:``) as can be the case in a standard ``.ini`` file. This is because some lone (no key) values such as remote vcf files may contain colons.
* The keys are read in a case-sensitive mode. This is to ensure file names added as lone values are interpreted correctly.

The genomic-config ``.ini`` file supports comments with a ``#``, although inline comments are not supported.

Section headings in a genomic-config ``.ini`` file
==================================================

Given the limitations of the ``.ini`` format, the hierarchy of the relationships is inferred in the section headings of the ``.ini`` file, with the various attributes delimited by a dot (``.``). The following attributes will be found in the various section headings:

1. heading type
2. species
3. genome assembly
4. file format
5. name

The heading type will appear in all the section headings, of the other attributes, they are applicable to specific section heading types. However, where they occur together, they will always be in the order they are listed above. The various attributes and their general rules are described in greater detail below.

.. _section_heading_types:

Section heading types
---------------------

The section heading type will always be the first component in the section heading. Currently, the heading types implemented in the genomic-config file are:

* ``general`` - General things that are useful to have "centrally", the only ``seed`` is available at present
* ``species_synonyms`` - Mappings between different ways of saying the same species name.
* ``assembly_synonyms`` - Mappings between different ways of saying the same genome assembly name.
* ``chain_files`` - The locations of chain files used to convert between genomic assemblies.
* ``reference_genome`` - The location of reference genome fasta files.
* ``mapping_file`` - This is a heading that is used by `gwas-norm <https://cfinan.gitlab.io/gwas_norm/index.html>`_. It defines a VCF file with variants and annotations across the whole genome, for details of the mapping file `see here <https://cfinan.gitlab.io/gwas_norm/mapping_files/mapping_files.html>`_
* ``cohort`` - The location of cohort files. Cohorts are collections of files containing individual level genotype data.
* ``samples`` - Lists of sample sets. Sample lists act as a central point of storing sample subsets tied to a name, for example ``1000_GENOMES_YRI`` that lists all the 1000 genomes Yoruba sample identifiers.

Section heading species
-----------------------

Some of the section heading types are associated with a particular species. For these, the species name will occur after the section heading type. Currently the section heading types that are tied to a species are:

* ``assembly_synonyms.<SPECIES>``
* ``chain_files.<SPECIES>``
* ``reference_genome.<SPECIES>``
* ``mapping_file.<SPECIES>``
* ``cohort.<SPECIES>``

Section heading assembly
------------------------

The same section type and species may exist for multiple genome assemblies. The section headings that require assembly information are:

* ``chain_files.<SPECIES>.<ASSEMBLY>``
* ``reference_genome.<SPECIES>.<ASSEMBLY>``
* ``mapping_file.<SPECIES>.<ASSEMBLY>``
* ``cohort.<SPECIES>.<ASSEMBLY>``

Section heading file format
---------------------------

Currently, the only section heading type that supports a file format in the section heading is the ``cohort`` (``cohort.<SPECIES>.<ASSEMBLY>.<FILE_TYPE>``). Whilst, the file format can be any string, genomic-config does associate specific file format types with index files that co-occur with the file format. Note that, in the genomic-config, a ``cohort`` can have exactly the same attributes, including the ``name`` (see below) but have a different format. Also, the definition of the file format in the section heading is case insensitive.

Section heading name
--------------------

Names are used to distinguish between sections that are otherwise identical for all their other attributes and can not support a name as a key in their option set. For example, there may exist multiple cohorts for human genome assembly GRCh38. In the section heading types that support a name, it will always be listed last in the section heading. Currently the section heading types that support a name are:

* ``cohort.<SPECIES>.<ASSEMBLY>.<FILE_TYPE>.<NAME>``
* ``samples.<NAME>``

Non-genomic-config section headings
===================================

It is not an error for the genomic-config file to contain other section headings apart from those described in :ref:`section headings <section_heading_types>`. The only caveat to this is that the non-genomic-config section headings must not start with any of the :ref:`defined <section_heading_types>` section headings followed by a dot (``.``). So, ``[reference_genome_assemblies]`` would be allowed but ``[reference_genome.assemblies]`` would produce an error.

Locations of index files
========================

Many of the genomics files in use today support an associated index file(s) that enable fast lookups from them. The convention is that these index files have the same basename as the genomic data file but with an additional file extension appropriate for their index. For example ``.bgen`` files can have an associated index file and these have ``.bgen.bgi`` file extensions.

Whilst, the convention is for similarly named data/index files, with many programs it is possible to have differently named data and index files. Unfortunately, currently the genomic-config ``.ini`` format does not support this as it will add too much complexity to the simple ``.ini`` file format. So, as it stands index files are assumed to have the same name as the data files but with their additional index extensions.

Currently, genomic-config only "knows" about index files associated with ``vcf`` / ``bcf`` files, ``bgen`` files and ``fasta`` files. As a default, when file formats that are associated with index files are defined; when the config file is parsed, the location of their index files is checked in addition to the data file, this behaviour can be switched off with ``validate_indexes=False`` when instantiating the parser.

Defining the configuration options
==================================

The ``general`` section
-----------------------

The general section contains general non-specific configuration options that might be useful in some circumstances. There should only be a single general section with the section heading ``general``.

Currently only a single option exists in general and that is for the definition of the random seed. This might be useful if you need a common location for your random seed. En example is shown below:

.. code-block:: ini

   [general]
   seed = 1984

The ``species_synonyms`` section
--------------------------------

The species synonyms section allows users to specify alternate names for the same species. This might be useful in a multi-user environment where different users might use different names for the same species.

The species synonyms should be stored under the header ``species_synonyms`` and there should be a single entry per config file.

The synonyms specified in the section are stored as ``key = value`` pairs with the structure ``alternate name = preferred name``.

The species synonyms are parsed before any other entries in the configuration file so they are available when all the other sections of the config file are parsed. If ``use_species_synonyms=True`` when the config file is parsed then all species definitions in other config sections are stored internally using the the preferred name for the species defined in the ``species_synonyms`` section. Similarly, when querying information from a section that is associated with a species, if ``use_species_synonyms=True`` then the query species is translated to the preferred name before the query takes place.

When using synonyms for species, there is a possibility that the user could specifiy the same section twice under different synonyms. For example, ``[cohort.human.b37.vcf.1KG_LOCAL]`` and ``[cohort.homo_sapiens.b37.vcf.1KG_LOCAL]``.  With ``use_species_synonyms=True`` this will produce an error as the same section is defined twice. An example of a species synonyms section is shown below:

.. code-block:: ini

   [species_synonyms]
   homo_sapiens = human
   homo sapiens = human
   humans = human

``assembly_synonyms`` sections
------------------------------

The assembly synonyms section works in a similar way to the species synonyms section, except that assembly synonyms are associated with a species.

Assembly synonyms should be prefixed with ``assembly_synonyms`` and you are allowed 1 per species. So the general structure should be ``assembly_synonyms.<SPECIES>``.

The synonyms specified in the section are stored as ``key = value`` pairs with the structure ``alternate name = preferred name``.

There is an option for ``use_assembly_synonyms`` which defaults to ``True``. As with species synonyms, this will translate both parsed assembly names and assembly queries into the preferred form.

An example of an assembly synonyms section is shown below:

.. code-block:: ini

   [assembly_synonyms.human]
   grch38 = b38
   b38 =    b38
   GRCh38 = b38
   hg38 =   b38
   grch37 = b37
   GRCh37 = b37
   b37 =    b37
   hg19 =   b37
   hg37 =   b37
   b36 =    b36
   hg18 =   b36
   hg36 =   b36
   NCBI36 = b36
   b35 =    b35
   hg17 =   b35
   hg35 =   b35
   NCBI35 = b35
   b34 =    b34
   hg16 =   b34
   hg34 =   b34
   NCBI34 = b34


``chain_files`` sections
------------------------

The chain files section allows the user to store the location of files that can be used to lift one genome assembly over to another genome assembly. The section heading must have the structure ``chain_files.<SPECIES>.<SOURCE_ASSEMBLY>``. The values in the section are key value pairs consisting of ``target assembly = chain file path``. As with other sections. The species, source/target assemblies are parsed to their preferred names when being queried, unless ``use_assembly_synonyms`` or ``use_species_synonyms`` are set to ``False``. An example of a chain file entry with source build GRCh38 and target assemblies, GRCh37, NCBI36, NCBI35, NCBI34.

.. code-block:: ini

   [chain_files.human.b38]
   b34 = /home/rmjdcfi/Dropbox/chain_files/GRCh38_to_NCBI34.chain.gz
   b35 = /home/rmjdcfi/Dropbox/chain_files/GRCh38_to_NCBI35.chain.gz
   b36 = /home/rmjdcfi/Dropbox/chain_files/GRCh38_to_NCBI36.chain.gz
   b37 = /home/rmjdcfi/Dropbox/chain_files/GRCh38_to_GRCh37.chain.gz

``reference_genomes`` sections
------------------------------

The reference genomes sections allows the user to store paths to reference genome assembly DNA sequences in fasta form. The section header has the structure ``reference_genome.<SPECIES>.<ASSEMBLY>``. The values in the section have the structure ``name = path to sequence file``. This allows for the user to give either a remote path or a local path to the same species/genome assembly. For example:

.. code-block:: ini

   [reference_genome.human.b38]
   remote = ftp://ftp.ensembl.org/pub/current_fasta/homo_sapiens/dna_index/Homo_sapiens.GRCh38.dna.toplevel.fa.gz
   local = /data/reference_genome/Homo_sapiens.GRCh38.dna.toplevel.fa.gz

``mapping_file`` section
------------------------

This is used by `gwas-norm <https://cfinan.gitlab.io/gwas_norm/index.html>`_. It defines a VCF file with variants and annotations across the whole genome, for details of the mapping file `see here <https://cfinan.gitlab.io/gwas_norm/mapping_files/mapping_files.html>`_. Although, it may also be of general use for variant lookups. The section heading has the structure ``mapping_file.<SPECIES>.<ASSEMBLY>``, with the values being ``name = path to the mapping file``. This allows for subtly different mapping file sets to be used depending on the task. An example of the mapping file section is shown below:

.. code-block:: ini

   [mapping_file.human.b37]
   all = /data/mapping_files/v20220402/gwas_norm.v20220402.biallelic.vep.b37.vcf.gz
   common = /data/mapping_files/v20220402/gwas_norm.v20220402.common.biallelic.vep.b37.vcf.gz
   rare = = /data/mapping_files/v20220402/gwas_norm.v20220402.rare.biallelic.vep.b37.vcf.gz 

``cohort`` sections
-------------------

The cohort section header must be of the structure ``[cohort.<SPECIES>.<ASSEMBLY>.<FILE_TYPE>.<NAME>]``. Each cohort section must be unique. However, it is possible to have the same cohort with a different name (if for some reason you want to) and to have a cohort with the same name but different file type and/or assembly. So for example, imagine you have the 1000 genomes as both VCF files and bgen files you could use ``[cohort.human.b37.vcf.1000g]`` and ``[cohort.human.b37.bgen.1000g]`` respectively.

Specifying genomic coverage of cohort input files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is common practice to split large genotype files across chunks of the genome, rather than having all the genome data in a single large file. A commonly schema is split across chromosome boundaries with a separate file for each chromosome. Occasionally, data is split even further with separate files for individual chunks of a chromosome, an example of this is the UK BioBank sequencing data.

Whilst this is efficient for storage and transfer of the files, it makes querying one or more genomic regions from the files problematic as the user must know how the data is stored and what genomic region is encapsulated by any specific file. The genomic-config cohort section aims to work around this problem by treating a cohort as a pool of files that the user can query with genomic regions and retrieve a set of files that encompass those regions.

Currently genomic-config relies on the user to tell it the coverage of each cohort file and does not auto detect this (i.e. through parsing the file and/or the index), although in future this will be added (although not a run time but via some sort of validation mechanism). However, it is relatively easy in most cases to define the genomic coverage of a cohort file.

This concept of genomic coverage is reflected in how the files for a cohort are detailed in a configuration file. The files can be listed using several methods. Most files listed in the configuration file will be regular local files, however, in the case of VCF files, they can also be in remote locations.

If the genotype data is split across multiple files, there needs to be a mechanism of detailing that in the configuration file. In genomic-config, this concept is called `hinting`, the user gives genomic-config a hint as to what genomic boundaries that variants within the file are contained within.

Hinting can be specified, explicitly or implicitly. If the file is local files, both explicit and implicit hinting can be used while remote locations can only be expressed explicitly hinted.

Explicit hinting
****************

Explicit hinting refers to detailing the region boundaries covered by the file as a key next to the file name within the configuration file. For example, ``chr1:120000-240000 = /my/genomic/chunk_chr1_from-120000_to-240000.vcf.gz``. The format of this is ``<chromosome name>:<start position (bp)>-<end position (bp)>``. The chromosome name should be represented exactly how it is in the file, so if it has no ``chr`` prefix, then it would look like ``1:120000-240000 = /my/genomic/chunk_chr1_from-120000_to-240000.vcf.gz``. If the file contains only data from a single chromosome, then it would look like ``chr1 = /my/genomic/chunk_chr1.vcf.gz`` or ``1 = /my/genomic/chunk_chr1.vcf.gz``

Current remote vcf locations have to be explicitly hinted as the code to query the remote location and glob the files is not in place yet. So the same file at a remote location would look like: ``1 = ftp://ftp.server.ac.uk/my/genomic/chunk.vcf.gz``.

Implicit hinting
****************

Implicit hinting refers to the region information being encoded in the actual file name. This can reduce the number of entries in the configuration file but may increase the chance of errors. The same region boundaries given in the example above would be expressed as a single value in the configuration file: ``/my/genomic/chunk_chr{CHR}_from-{START}_to-{END}.vcf.gz``. The chromosome name, start coordinates and end coordinates are represented by the placeholders ``{CHR}``, ``{START}`` and ``{END}`` respectively. These are expanded when the configuration file is parsed. If the files are several different hinting structures then more templates can be included in the cohort section. If the file contains all variants from the single chromosome then the ``{START}`` and ``{END}`` placeholders are omitted, for example: ``/my/genomic/chunk_chr{CHR}.vcf.gz``. Currently, only ``{CHR}`` or full ``{CHR}``, ``{START}`` and ``{END}`` placeholders are supported and subsets of them are not, i.e. ``{CHR}``, ``{START}`` and no ``{END}``.

Currently, multiple locations are not supported either explicitly or implicitly, so a file with data from two chromosomes or disjoint regions can't be specified directly. Only single chromosomes and contiguous regions are supported, this may change in future but is low priority as it is unclear how often this will be used. You can work around this by specifying twice if needed, for example: ``/my/genomic/chunk_on_chr{CHR}_and_chr2.vcf.gz`` and ``/my/genomic/chunk_on_chr1_and_chr{CHR}.vcf.gz``.

If all the variants are in a single file, then no hinting should be specified. However, if the genotype data is split over multiple files then you should detail the split as explicit hinting or implicit hinting are it will dramatically speed up the query times for genotypes overlapping regions.

Finally, within the cohort section of the configuration file, all file paths must be specified as absolute file paths.

Care should be taken when using implicit hinting as all the files matching the file structure are globbed from the directory name of the implicitly hinted file, so there is a possibility to select files that you do not want. Additionally, mixing of hinting types within a directory could be problematic. For example, if you have files with ``/data/genomic_region_chr{CHR}.vcf.gz`` and ``/data/genomic_region_chr{CHR}_{START}_{END}.vcf.gz`` within the ``/data`` directory, then implicit hinting would capture both files correctly but it will also capture a second copy of the second file but with a chromosome name of ``{CHR}_{START}_{END}``, so it would incorporate the start and positions within the chromosome name. This should not be a problem if only a single hinting pattern is in any single directory but if you are in any doubt then you should explicitly hint files.

Examples of a ``cohort`` section
********************************

.. code-block:: ini

   # Implicit hinting of local 1000 genomes VCFs
   [cohort.human.b37.vcf.1KG_LOCAL]
   /data/1000g/b37/vcfs/ALL.chr{CHR}.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   /data/1000g/b37/vcfs/ALL.chr{CHR}.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz
   /data/1000g/b37/vcfs/ALL.chr{CHR}.phase3_integrated_v2a.20130502.genotypes.vcf.gz
   /data/1000g/b37/vcfs/ALL.chr{CHR}.phase3_callmom-v0_4.20130502.genotypes.vcf.gz

   # Explicit hinting of remote 1000 genomes VCFs
   [cohort.human.b37.vcf.1KG_REMOTE]
   1 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr1.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   2 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   3 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr3.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   4 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr4.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   5 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr5.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   6 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr6.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   7 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr7.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   8 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr8.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   9 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr9.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   10 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr10.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   11 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr11.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   12 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr12.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   13 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr13.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   14 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr14.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   15 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr15.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   16 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr16.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   17 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr17.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   18 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr18.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   19 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr19.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   20 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr20.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   21 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr21.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   22 = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
   X = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz
   Y = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz
   MT = ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz

``chr_name_synonyms`` sections
------------------------------

The chromosome synonyms sections allows you to specify mappings between different chromosome naming conventions. This fits neatly into the ini file format. The sections should be specified with a species, genome assembly and a name. So different sections can be applied to different scenarios.

When defining the mappings the source name should be given first (the key) and the destination name (i.e. what you want) should be given second. These should eb separated with an equals sign as per the adjusted ini specification given above.

.. code-block:: ini

   [chr_name_synonyms.human.b38.ALL]
   1            = 1
   2            = 2
   3            = 3
   4            = 4
   5            = 5
   6            = 6
   7            = 7
   8            = 8
   9            = 9
   10           = 10
   11           = 11
   12           = 12
   13           = 13
   14           = 14
   15           = 15
   16           = 16
   17           = 17
   18           = 18
   19           = 19
   20           = 20
   21           = 21
   22           = 22
   23           = X
   24           = Y
   25           = MT
   M            = MT
   NC_000001.11 = 1
   NC_000002.12 = 2
   NC_000003.12 = 3
   NC_000004.12 = 4
   NC_000005.10 = 5
   NC_000006.12 = 6
   NC_000007.14 = 7
   NC_000008.11 = 8
   NC_000009.12 = 9
   NC_000010.11 = 10
   NC_000011.10 = 11
   NC_000012.12 = 12
   NC_000013.11 = 13
   NC_000014.9  = 14
   NC_000015.10 = 15
   NC_000016.10 = 16
   NC_000017.11 = 17
   NC_000018.10 = 18
   NC_000019.10 = 19
   NC_000020.11 = 20
   NC_000021.9  = 21
   NC_000022.11 = 22
   NC_000023.11 = X
   NC_000024.10 = Y
   NC_012920.1  = MT

``samples`` sections
--------------------

The sample section allows the user to store commonly used sample sets under a single name. This is useful if a cohort has mixed population ancestries, such as the 1000 genomes or UK BioBank. The sample identifiers for each ancestry can be stored under a population name and can be retrieved and used as needed from the config file.

Sample set section headings should have the prefix ``samples.`` and the samples identifiers should be stored as one identifier per row and are case sensitive. It is an error for a sample identifier to have a value associated with it.

Sample identifiers are not associated with any species or genome assemblies they exist as distinct sections within the config file. Some examples are shown below.

.. code-block:: ini

   [samples.1KG_AFR]
   HG01879
   HG01880
   HG01882
   HG01883
   HG01885
   HG01886
   HG01889
   HG01890
   # truncated...

   [samples.1KG_EAS]
   HG00403
   HG00404
   HG00406
   HG00407
   HG00409
   HG00410
   HG00419
   HG00421
   # truncated...

   [samples.1KG_EUR]
   HG00096
   HG00097
   HG00099
   HG00100
   HG00101
   HG00102
   HG00103
   HG00105
   HG00106
   # truncated...

   [samples.1KG_CEU]
   NA06984
   NA06985
   NA06986
   NA06989
   NA06994
   NA07000
   NA07037
   NA07048
   # truncated...

