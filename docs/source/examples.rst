Example code
============

Here is some example code demonstrating how to use the various elements of the package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   examples/using_genomic_config
   
