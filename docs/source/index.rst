.. genomic-config documentation master file, created by
   sphinx-quickstart on Tue April 20 11:45:00 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to genomic-config
=========================

The genomic-config package provides an interface to configuration files that provide the locations for various genomic based files that exist on a system. It can be used by external programs and python code to provide a central point to locate the files. Currently, it only supports `ini` files but will be expanded to incorporate other configuration file types in the future. Currently it provides access to:

1. Reference genome assemblies (fasta files)
2. Reference genotype files (i.e. 1000 genomes)
3. Chain files for liftovers
4. Sample lists

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: genomic-config file formats

   config_files

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing
   package_management


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
