genomic-config files
====================

The rationale for the config file is to make it easy for users to find files and options that are useful for accessing genotypes from reference populations, chain files for lifting over and reference genomes for validating the reference allele at a site. Useful sample sets can also be stored in a reference file.

Once you have defined a reference file, most of the time you will not need to directly interact with it. However, you can if you want to and the examples section gives some details of interacting with a configuration file.

This guide will take you through some of the configuration options and will allow you to define a configuration file of your own.

Location
--------

The configuration file can be located anywhere on your system and the path provided to the configuration parser. However, there are also some default locations are looked in should a configuration file path not be available.

* ``GENOMIC_CONFIG`` - This environment variable can be set in your ``~/.bashrc`` or ``~/.bash_profile`` file and can point to the location of the genomic configuration file. For example:
  ``export GENOMIC_CONFIG="${HOME}/path/to/.genomic_data.cnf"``. In the shell this is then available as ``$GENOMIC_CONFIG``.
* ``~/.genomic_data.cnf`` - If no environment variable is set then the root of your home directory is checked for a hidden file called ``.genomic_data``, this can have any of the file extensions ``.cnf``, ``.ini``, ``.cfg`` (checked in that order).

If you have not manually specified the configuration file path and the file is not available at either of these two locations, then a `FileNotFoundError` will be raised.

Configuration format
--------------------

Currently the ``.ini`` file format is the only format you can write your configuration file in. We hope to add, ``.yml``, ``.json`` and probably ``.xml`` in future.

.. toctree::
   :maxdepth: 2

   config_files/ini_files
