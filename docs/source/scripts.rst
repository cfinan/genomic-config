======================
Command-line endpoints
======================

Below is a list of all the command line endpoints installed with the package. Currently, there is only one.

``genomic-config``
------------------

.. _genomic_config_cmd:

.. argparse::
   :module: genomic_config.genomic_config
   :func: _init_cmd_args
   :prog: genomic-config
