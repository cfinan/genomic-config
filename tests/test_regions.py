"""Tests for the `genomic_config.regions` module
"""
from genomic_config import regions
import pytest
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # The variable names by which the parameters in the list will be
    # available in the test function
    "test_region, result",
    # Each element in this list represents a single parameter
    [
        (('5', 1, 1), ['file1.txt', 'file2.txt', 'file7.txt']),
        (('1', 1, 1), ['file1.txt', 'file2.txt', 'file3.txt', 'file10.txt']),
        (('2', 1, 10000), ['file1.txt', 'file2.txt', 'file4.txt']),
        (
            ('1', 400, 2500),
            ['file1.txt', 'file2.txt', 'file3.txt', 'file10.txt',
             'file12.txt', 'file15.txt',  'file11.txt']
        ),
        (
            ('1', 1, 1000),
            ['file1.txt', 'file2.txt', 'file3.txt', 'file10.txt',
             'file15.txt']
        )
    ]
)
def test_add_regions_from_list(region_defs, test_region, result):
    """Test that `genomic_config.regions.RegionFileFetch` works as expected.
    This uses ``region_defs`` fixture and some pre-defined search regions and
    results based on ``region_defs``.
    """
    file_holder = regions.RegionFileFetch(from_list=region_defs)
    assert sorted(file_holder.get_files(*test_region)) == sorted(result), \
        "wrong files"
