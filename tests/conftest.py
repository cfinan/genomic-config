"""Fixtures (re-usable data components and settings) for tests
"""
from genomic_config import base_config
from genomic_config.example_data import examples
import os
import pytest
import tempfile


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def tmpfile(tmpdir):
    """Return a temp file.
    """
    tmp_handle, tmp_path = tempfile.mkstemp(dir=tmpdir)
    os.close(tmp_handle)
    return tmp_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def config_sections():
    """Return a list of configuration file sections for any tests that have to
    iterate over them.
    """
    return [
        base_config.GENERAL_SECTION, base_config.SAMPLE_PREFIX
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def species_sections():
    """Return a list of configuration file sections for any tests that have to
    iterate over them.
    """
    return [
        base_config.ASSEMBLY_SYN_PREFIX, base_config.CHAIN_FILE_PREFIX,
        base_config.ASSEMBLY_PREFIX, base_config.COHORT_PREFIX
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def config_file():
    """Return the path to a usable configuration file.
    """
    return examples.get_data("config_file_path")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def full_config_file(tmpdir):
    """Return the path to a usable configuration file.
    """
    return examples.get_data("config_file_copy", tmpdir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def missing_chain(tmpdir):
    """Return the path to a usable configuration file where the chain files are
    defined but do not exist.
    """
    return examples.get_data(
        "config_file_copy", tmpdir, create_chain_files=False,
        basename="no_chain.cnf"
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def chain_assemblies():
    """Return some assemblies used in the chain file section of the full config
    file.
    """
    return examples.get_data("test_genome_assemblies")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def expected_samples():
    """The expected sample names and the numbers of samples they contain
    """
    return sorted(
        [('1KG_AFR', 661), ('1KG_AMR', 347), ('1KG_EAS', 504), ('1KG_EUR', 503),
         ('1KG_SAS', 489), ('1KG_ACB', 96),  ('1KG_ASW', 61),  ('1KG_BEB', 86),
         ('1KG_CDX', 93),  ('1KG_CEU', 99),  ('1KG_CHB', 103), ('1KG_CHS', 105),
         ('1KG_CLM', 94),  ('1KG_ESN', 99),  ('1KG_FIN', 99),  ('1KG_GBR', 91),
         ('1KG_GIH', 103), ('1KG_GWD', 113), ('1KG_IBS', 107), ('1KG_ITU', 102),
         ('1KG_JPT', 104), ('1KG_KHV', 99),  ('1KG_LWK', 99),  ('1KG_MSL', 85),
         ('1KG_MXL', 64),  ('1KG_PEL', 85),  ('1KG_PJL', 96),  ('1KG_PUR', 104),
         ('1KG_STU', 102), ('1KG_TSI', 107), ('1KG_YRI', 108)],
        key=lambda x: x[0]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def implicit_hint_chr(tmpdir):
    """Generate some files to match an implicit hinting pattern
    """
    chrs = ['1', '2', '3', '4', '5', 'X', 'Y', 'MT', 'CHR_HSCHR1_3_CTG32_1']
    base_hint = 'test_hint_file_chr{CHR}.vcf.gz'
    base_file = os.path.join(tmpdir, base_hint)
    results = []
    files = []
    for i in chrs:
        fn = base_file.format(CHR=i)
        open(fn, 'w').close()
        results.append((i, None, None))
        files.append(fn)
    yield base_file, results
    for i in files:
        os.unlink(i)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def implicit_hint_full(tmpdir):
    """Generate some files to match an implicit hinting pattern, with full
    chromosome, start and end position hinting.
    """
    results = [
        ('1', 1, 1000),
        ('1', 1001, 2000),
        ('1', 2001, 3000),
        ('1', 3001, 4000),
        ('1', 4001, 5000),
        ('1', 5001, 6000),
    ]
    base_hint = 'test_hint_file_chr{CHR}_start_at{START}_{END}.vcf.gz'
    base_file = os.path.join(tmpdir, base_hint)
    files = []
    for chr_name, start_pos, end_pos in results:
        fn = base_file.format(CHR=chr_name, START=start_pos, END=end_pos)
        open(fn, 'w').close()
        files.append(fn)
    yield base_file, results
    for i in files:
        os.unlink(i)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def region_defs():
    return [
        ((None, None, None), 'file1.txt'),
        ((None, None, None), 'file2.txt'),
        (('1', None, None), 'file3.txt'),
        (('2', None, None), 'file4.txt'),
        (('3', None, None), 'file5.txt'),
        (('4', None, None), 'file6.txt'),
        (('5', None, None), 'file7.txt'),
        (('6', None, None), 'file8.txt'),
        (('7', None, None), 'file9.txt'),
        (('1', 1, 1000), 'file10.txt'),
        (('1', 1001, 2000), 'file11.txt'),
        (('1', 2001, 3000), 'file12.txt'),
        (('1', 3001, 4000), 'file13.txt'),
        (('1', 4001, 5000), 'file14.txt'),
        (('1', 500, 1500), 'file15.txt')
    ]
