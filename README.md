# Getting Started with genomic-config
__version__: `0.1.3b0`

The genomic-config package provides an interface to configuration files that provide the locations for various genomic based files that might exist on a system. It can be used by external programs and Python code to provide a central point to locate the files. Currently, it only supports `ini` files but will be expanded to incorporate other configuration file types in the future.

genomic-config provides access to:

1. Reference genome assemblies (fasta files)
2. Reference genotype files (i.e. 1000 genomes)
3. Chain files for liftovers
4. Sample lists
5. variant annotation/mapping VCF files
6. mappings between chromosome synonyms

There is [online](https://cfinan.gitlab.io/genomic-config/index.html) documentation.

## Installation instructions
At present, genomic-config is undergoing development and no packages exist yet on PyPy. I maintain a build in my [conda channel](https://anaconda.org/cfin) that I will try to keep updated to the latest Python versions back to Python 3.8.

Also, please note that the conda builds are for 64 bit Linux and Mac only. However, all development is performed on 64 bit Linux under Python 3.8. There are pytests available if you want to test for errors.

With this in mind there are several installation options.

### Installation using the conda build
This uses my personal conda channel and conda-forge to install. However, keep in mind to update regularly.
```
conda install -c cfin -c conda-forge genomic-config
```

### Installation using git
First, clone this repository and then `cd` to the root of the repository.

```
git clone git@gitlab.com:cfinan/genomic-config.git
cd gwas_norm
```

### Installation using git not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip`:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

To install run the command below from the root of the genomic-config repository:
```
python -m pip install .
```

## Next steps...
If you have cloned the repository, after installation, you may want to run the tests:

1. Run the tests using ``pytest ./tests`` - If you have installed using conda, you will need to clone the repository using git to run the tests.
2. [Setup](https://cfinan.gitlab.io/genomic-config/config_files.html) your configuration file.
